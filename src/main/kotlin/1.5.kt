//1.5 D’String a MutableList<Char>

fun stringToChar(word:String): MutableList<Char>{
    var mutableList = mutableListOf<Char>()
    word.forEach {
        mutableList += it
    }
    return mutableList
}