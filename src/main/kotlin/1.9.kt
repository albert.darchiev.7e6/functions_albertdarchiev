//1.9 Escriu una creu

fun crossPrint(num1:Int, char:Char): String {
    var space = ""
    repeat(num1/2){space+= " "}
    var printRES = ""
    for (i in 0 until num1){
        if (i != num1/2) printRES +=("$space$char\n" )
        else {
            repeat(num1) { printRES += ("$char") }
            printRES += ("\n")
        }
    }
    return printRES.toString()
}