//1.1 Mida d’un String

fun stringLength(word1: String): Int {
    var counter = 0
    word1.forEach { counter++ }
    return counter
}
