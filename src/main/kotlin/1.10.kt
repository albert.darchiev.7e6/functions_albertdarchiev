//1.10 Escriu un marc per un String

fun textDecor(word: String, char:Char): String {
    var result = ""

    repeat(word.length+4) { result += "$char"}
    result += "\n"                                      //LINE 1

    result += "$char "
    repeat(word.length){result+= " "}                   //LINE 2
    result += " $char\n"

    result += "$char $word $char\n"                     //LINE 3

    result += "$char "
    repeat(word.length){result+= " "}                   //LINE 4
    result += " $char\n"

    repeat(word.length+4) { result += "$char"}    //LINE 5
    result += "\n"



    return result
}