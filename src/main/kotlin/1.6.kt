//1.6 Divideix un String

fun splitString(word:String, letter:Char):MutableList<String>{
    return word.split(letter).toMutableList()
}