//1.4 Subseqüència

fun checkSubsequence(word: String, start: Int, end:Int): String{
    var list = ""
    for( i in start until end){
        list += word[i].toString()
    }
    if (word.length < end) return ("La subsequencia $start-$end no existeix")
    else return list
}