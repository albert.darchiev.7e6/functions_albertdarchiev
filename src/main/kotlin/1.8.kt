//1.8 Escriu una línia

fun linePrint(num1:Int,num2:Int,char:Char):String{
    var result = ""

    repeat(num1){
        result += " "
    }
    repeat(num2){
        result+=char
    }
    return result
}