//1.3 Caràcter d’un String

fun checkPosition(word: String, position:Int):String{
    if (word.length < position) return ("La mida del String es inferior a $position")
    else return word[position].toString()
}