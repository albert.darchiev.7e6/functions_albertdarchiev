
fun main() {
    val word1 = "print example"
    val word2 = "print example"
    val num1 = 7
    val num2 = 7
    val char = 'a'

    println(stringLength(word1))                //1.1
    println(compareString(word1, word2))        //1.2
    println(checkPosition(word1, num1))         //1.3
    println(checkSubsequence(word1,num1,num2))  //1.4
    println(stringToChar(word1))                //1.5
    println(splitString(word1, char))           //1.6
    println(powFuncion(num1, num2))             //1.7
    println(linePrint(num1,num2,char))          //1.8
    println(crossPrint(num1, char))             //1.9
    println(textDecor(word1, char))             //1.10


}
